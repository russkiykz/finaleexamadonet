﻿using Hospital.Data;
using Hospital.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hospital.UI
{
    public abstract class Menu
    {
        public static void Start()
        {
            var therapist = new Doctor { FullName = "Иванов В.В.", Profession = Profession.Therapist};
            var surgeon = new Doctor { FullName = "Петров А.С.", Profession = Profession.Surgeon };
            var dentist = new Doctor { FullName = "Сидоров П.П.", Profession = Profession.Dentist };
            var ophthalmologist = new Doctor { FullName = "Кунанбаев А.Ж.", Profession = Profession.Ophthalmologist };
            var cardiologist = new Doctor { FullName = "Куанышбаев Л.К.", Profession = Profession.Cardiologist };

            

            List<Doctor> doctors;
            using(var context = new HospitalContext())
            {
                //context.AddRange(therapist, surgeon, dentist, ophthalmologist, cardiologist);
                //context.SaveChanges();
                doctors = context.Doctors.ToList();
            }

            Console.WriteLine("Добро пожаловать в нашу клинику!\n\nПредставьтесь пожалуйста: ");
            var patient = new Patient { Name = Console.ReadLine() };
            Console.Write("Укажите Ваш возраст: ");
            int.TryParse(Console.ReadLine(), out int age);
            patient.Age = age;
            Console.WriteLine("Укажите Ваш пол:\n1. Мужской\n2. Женский");
            switch (Console.ReadLine())
            {
                case "1":
                    patient.Gender = Gender.Male;
                    break;
                case "2":
                    patient.Gender = Gender.Female;
                    break;
            }

            using (var context = new HospitalContext())
            {
                context.Add(patient);
                context.SaveChanges();
            }

            var numberPosition = 0;
            Console.WriteLine("\nНаши специалисты: ");
            foreach(var doctor in doctors)
            {
                Console.Write($"{++numberPosition}. ");
                Console.WriteLine($"{doctor.FullName} - { doctor.Profession}");
            }
            numberPosition = 0;


            while (true)
            {
                Console.WriteLine("\nВыберите необходимого специалиста или нажмите 0 для выхода");
                string position = Console.ReadLine();
                var timeWork = new DateTime(2021, 03, 18, 09, 0, 0);
                switch (position)
                {
                    
                    case "1":
                        Console.Clear();
                        var shedules = new List<Shedule>();
                        using(var context = new HospitalContext())
                        {
                            shedules = context.Shedules.ToList();
                        }
                        var listTimeWork = new List<DateTime>();
                        Console.WriteLine("Доступное время: ");
                        var count = 0;
                        while(timeWork!= new DateTime(2021, 03, 18, 18, 0, 0))
                        {
                            if (shedules.Count==0 || shedules[count].TimeOfReceipt == timeWork)
                            {                                
                                count++;
                            }
                            else
                            {
                                Console.WriteLine($"{++numberPosition}. {timeWork.ToShortTimeString()}");
                                listTimeWork.Add(timeWork);
                            }
                            timeWork = timeWork.AddMinutes(30);
                        }
                        listTimeWork.Add(timeWork);
                        Console.Write("Выберите подходящее время(номер в расписании): ");
                        int.TryParse(Console.ReadLine(), out int number);
                        var recording = new Shedule { DoctorId = doctors[0].Id, PatientId=patient.Id,TimeOfReceipt=listTimeWork[number-1]};
                        using (var context = new HospitalContext())
                        {
                            context.Add(recording);
                            context.SaveChanges();
                        }
                        numberPosition = 0;
                        count = 0;
                        break;
                    case "0":
                        return;
                }
            }

        }

    }
}
