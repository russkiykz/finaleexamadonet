﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hospital.Migrations
{
    public partial class AddNaming : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shedules_Doctors_DoctorId",
                table: "Shedules");

            migrationBuilder.DropForeignKey(
                name: "FK_Shedules_Patients_PatientId",
                table: "Shedules");

            migrationBuilder.RenameColumn(
                name: "TimeOfReceipt",
                table: "Shedules",
                newName: "timeOfReceipt");

            migrationBuilder.RenameColumn(
                name: "PatientId",
                table: "Shedules",
                newName: "patientId");

            migrationBuilder.RenameColumn(
                name: "DoctorId",
                table: "Shedules",
                newName: "doctorId");

            migrationBuilder.RenameIndex(
                name: "IX_Shedules_PatientId",
                table: "Shedules",
                newName: "IX_Shedules_patientId");

            migrationBuilder.RenameIndex(
                name: "IX_Shedules_DoctorId",
                table: "Shedules",
                newName: "IX_Shedules_doctorId");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Patients",
                newName: "name");

            migrationBuilder.RenameColumn(
                name: "Gender",
                table: "Patients",
                newName: "gender");

            migrationBuilder.RenameColumn(
                name: "Age",
                table: "Patients",
                newName: "age");

            migrationBuilder.RenameColumn(
                name: "Profession",
                table: "Doctors",
                newName: "profession");

            migrationBuilder.RenameColumn(
                name: "FullName",
                table: "Doctors",
                newName: "fullName");

            migrationBuilder.AddForeignKey(
                name: "FK_Shedules_Doctors_doctorId",
                table: "Shedules",
                column: "doctorId",
                principalTable: "Doctors",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Shedules_Patients_patientId",
                table: "Shedules",
                column: "patientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shedules_Doctors_doctorId",
                table: "Shedules");

            migrationBuilder.DropForeignKey(
                name: "FK_Shedules_Patients_patientId",
                table: "Shedules");

            migrationBuilder.RenameColumn(
                name: "timeOfReceipt",
                table: "Shedules",
                newName: "TimeOfReceipt");

            migrationBuilder.RenameColumn(
                name: "patientId",
                table: "Shedules",
                newName: "PatientId");

            migrationBuilder.RenameColumn(
                name: "doctorId",
                table: "Shedules",
                newName: "DoctorId");

            migrationBuilder.RenameIndex(
                name: "IX_Shedules_patientId",
                table: "Shedules",
                newName: "IX_Shedules_PatientId");

            migrationBuilder.RenameIndex(
                name: "IX_Shedules_doctorId",
                table: "Shedules",
                newName: "IX_Shedules_DoctorId");

            migrationBuilder.RenameColumn(
                name: "name",
                table: "Patients",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "gender",
                table: "Patients",
                newName: "Gender");

            migrationBuilder.RenameColumn(
                name: "age",
                table: "Patients",
                newName: "Age");

            migrationBuilder.RenameColumn(
                name: "profession",
                table: "Doctors",
                newName: "Profession");

            migrationBuilder.RenameColumn(
                name: "fullName",
                table: "Doctors",
                newName: "FullName");

            migrationBuilder.AddForeignKey(
                name: "FK_Shedules_Doctors_DoctorId",
                table: "Shedules",
                column: "DoctorId",
                principalTable: "Doctors",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Shedules_Patients_PatientId",
                table: "Shedules",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
