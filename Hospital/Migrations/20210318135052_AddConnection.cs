﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hospital.Migrations
{
    public partial class AddConnection : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Id",
                table: "Doctors",
                newName: "id");

            migrationBuilder.CreateIndex(
                name: "IX_Shedules_PatientId",
                table: "Shedules",
                column: "PatientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Shedules_Patients_PatientId",
                table: "Shedules",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shedules_Patients_PatientId",
                table: "Shedules");

            migrationBuilder.DropIndex(
                name: "IX_Shedules_PatientId",
                table: "Shedules");

            migrationBuilder.RenameColumn(
                name: "id",
                table: "Doctors",
                newName: "Id");
        }
    }
}
