﻿using Hospital.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Hospital.Data
{
    public class HospitalContext:DbContext
    {
        public HospitalContext():base()
        {
            Database.Migrate();
        }

        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Shedule> Shedules { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=DESKTOP-0V33MK0\\MSSQLSERVER01; Database = HospitalBD; Trusted_Connection=true;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Doctor>()
                .Property(doctor => doctor.Id)
                .HasColumnName("id");
            modelBuilder.Entity<Doctor>()
                .Property(doctor => doctor.FullName)
                .HasColumnName("fullName");
            modelBuilder.Entity<Doctor>()
                .Property(doctor => doctor.Profession)
                .HasColumnName("profession");

            modelBuilder.Entity<Patient>()
                .Property(patient => patient.Name)
                .HasColumnName("name");
            modelBuilder.Entity<Patient>()
                .Property(patient => patient.Age)
                .HasColumnName("age");
            modelBuilder.Entity<Patient>()
                .Property(patient => patient.Gender)
                .HasColumnName("gender");
            
            modelBuilder.Entity<Shedule>()
                .Property(patient => patient.DoctorId)
                .HasColumnName("doctorId");
            modelBuilder.Entity<Shedule>()
                .Property(patient => patient.PatientId)
                .HasColumnName("patientId");
            modelBuilder.Entity<Shedule>()
                .Property(patient => patient.TimeOfReceipt)
                .HasColumnName("timeOfReceipt");

            base.OnModelCreating(modelBuilder);
        }
    }
}
