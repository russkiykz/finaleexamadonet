﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Models
{
    public class Patient:Entity
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Gender Gender { get; set; }
    }
}
