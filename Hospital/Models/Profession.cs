﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Models
{
    public enum Profession
    {
        Therapist,
        Surgeon,
        Dentist,
        Ophthalmologist,
        Cardiologist
    }
}
