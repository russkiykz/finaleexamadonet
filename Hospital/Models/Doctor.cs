﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hospital.Models
{
    public class Doctor:Entity
    {
        public string FullName { get; set; }
        public Profession Profession { get; set; }
        public ICollection<Shedule> Shedules { get; set; }
    }
}
