﻿using System;

namespace Hospital.Models
{
    public class Shedule:Entity
    {
        public Guid DoctorId { get; set; }
        public virtual Doctor Doctor { get; set; }
        public Guid PatientId { get; set; }
        public virtual Patient Patient { get; set; }
        public  DateTime TimeOfReceipt { get; set; }
    }
}